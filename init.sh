#!/bin/bash

# Update and upgrade of the system
sudo apt -y update
sudo apt -y upgrade

# Whiptail installation
sudo apt install -y whiptail

# Pip installation and whiptail and tqdm installation
sudo apt install -y pip
python3 -m pip install whiptail-dialogs --user --break-system-packages
python3 -m pip install tqdm --user --break-system-packages

# Reducting script
/bin/python3 python/reduction.py

# ANSSI and various recommandations security script
/bin/python3 python/securityScript.py
/bin/python3 python/lynisAuditSecurisationScript.py

# VPN configuration
/bin/python3 python/vpn.py

# Replace interfaces with the ones in the interface file
sudo cp other/interfaces /etc/network/interfaces

# Execute the firewall script
sudo bash other/iptables.sh

# Add the Google DNS to the resolv.conf file
sudo echo "nameserver 8.8.8.8" >> /etc/resolv.conf
sudo echo "nameserver 8.8.4.4" >> /etc/resolv.conf

# Execution of the nextcloud.sh script
sudo bash nextcloud.sh

# Audit the system
sudo apt install -y lynis
sudo lynis audit system

# Pause of 10 seconds to read the conclusion of the audit
sleep 10

# Restart the system
sudo reboot