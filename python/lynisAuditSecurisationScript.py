import subprocess
from whiptail import Whiptail
from tqdm import tqdm

def secure_sudoers_d():
    """
    Secure /etc/sudoers.d by setting appropriate permissions.
    """
    try:
        subprocess.run(['sudo', 'chmod', '750', '/etc/sudoers.d'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def secure_conf_files():
    """
    Secure configuration files using sysctl.
    """
    try:
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.dmesg_restrict=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.kptr_restrict=2'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.modules_disabled=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.perf_event_paranoid=3'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.unprivileged_bpf_disabled=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.core.bpf_jit_harden=2'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv6.conf.all.accept_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv6.conf.default.accept_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.core_uses_pid=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'dev.tty.ldisc_autoload=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'fs.protected_fifos=2'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.sysrq=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def install_libpam_tempdir():
    """
    Install libpam tempdir.
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'libpam-tmpdir', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def install_needrestart():
    """
    Install needrestart.
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'needrestart', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def disable_core_dumps():
    """
    Disable core dumps.
    """
    try:
        subprocess.run(['sudo', 'sh', '-c', 'echo "fs.suid_dumpable = 0" >> /etc/sysctl.conf'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sh', '-c', 'echo "kernel.core_pattern = |/bin/false" >> /etc/sysctl.conf'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'fs.suid_dumpable=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'kernel.core_pattern=|/bin/false'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-p', '/etc/sysctl.conf'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

units_to_hardening = [
    "clamav-freshclam.service",
    "cron.service",
    "dbus.service",
    "emergency.service",
    "exim4-base.service",
    "fail2ban.service",
    "lynis.service",
    "rescue.service",
    "systemd-ask-password-console.service",
    "systemd-ask-password-wall.service",
    "systemd-fsckd.service",
    "systemd-initctl.service",
    "systemd-rfkill.service",
]

def hardening_units():
    """
    Harden systemd units.
    """
    try:
        for unit in tqdm(units_to_hardening, desc="Hardening units", ncols=100):
            str_file = "/etc/systemd/system/" + unit
            str_new_file = "/home/secpicloud/sae3.01-secpicloud/new_conf_files/" + unit
            subprocess.run(['sudo', 'cp', '-f', str_new_file, str_file], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def correct_login_defs():
    """
    Correct /etc/login.defs settings.
    """
    try:
        # Configure password hashing rounds (SHA512)
        subprocess.run(['sudo', 'sh', '-c', 'echo "ENCRYPT_METHOD SHA512" >> /etc/login.defs'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sh', '-c', 'echo "SHA_CRYPT_MIN_ROUNDS 10000" >> /etc/login.defs'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sh', '-c', 'echo "SHA_CRYPT_MAX_ROUNDS 50000" >> /etc/login.defs'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        # Set password expire dates for secpicloud
        subprocess.run(['sudo', 'chage', '--maxdays', '365', '-m', '7', '-W', '14', 'secpicloud'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        # Configure minimum and maximum password age
        subprocess.run(['sudo', 'sh', '-c', 'echo "PASS_MIN_DAYS 7" >> /etc/login.defs'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sh', '-c', 'echo "PASS_MAX_DAYS 90" >> /etc/login.defs'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        # Set stricter default umask
        subprocess.run(['sudo', 'sh', '-c', 'echo "UMASK 027" >> /etc/login.defs'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def install_debsums():
    """
    Install debsums.
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'debsums', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def install_apt_show_versions():
    """
    Install apt-show-versions.
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'apt-show-versions', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def enable_auditd():
    """
    Enable auditd.
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'auditd', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        # Place the audit.rules file in the /etc/audit/ directory
        subprocess.run(['sudo', 'cp', '/home/secpicloud/sae3.01-secpicloud/others/audit.rules', '/etc/audit/audit.rules'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def install_integrity_tool():
    """
    Install integrity tool (AIDE).
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'aide', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        # Initialize AIDE
        subprocess.run(['sudo', 'aideinit'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'cp', '/var/lib/aide/aide.db.new', '/var/lib/aide/aide.db'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def enable_accounting():
    """
    Enable accounting.
    """
    try:
        # With accton
        subprocess.run(['sudo', 'apt', 'install', 'acct', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        # With systemd
        subprocess.run(['sudo', 'systemctl', 'enable', 'acct'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'systemctl', 'start', 'acct'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

        # Check if accounting is enabled
        subprocess.run(['sudo', 'ac', '-p'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error: {e}")

def main():
    """
    Main function to execute Lynis audit steps.
    """
    w = Whiptail(title="Lynis audit", backtitle="Lynis audit")
    choice, ok = w.menu("Choose the execution mode :", [("Auto", "Securise predefined services."), ("Manual", "Select which services should be securised manually.")])
    if ok:
        return
    
    if choice == "Auto":
        steps = [
            secure_sudoers_d,
            secure_conf_files,
            install_libpam_tempdir,
            install_needrestart,
            disable_core_dumps,
            hardening_units,
            correct_login_defs,
            install_debsums,
            install_apt_show_versions,
            enable_auditd,
            enable_accounting,
            # install_integrity_tool,
        ]

        for step in tqdm(steps, desc="Applying security measures", ncols=100):
            step()

        print("Success: Security measures applied.")
    else:
        print("Manual mode not implemented yet.")

if __name__ == "__main__":
    main()
