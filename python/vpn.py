import subprocess
from tqdm import tqdm

def install_wireguard():
    try:
        subprocess.run(['apt', 'update'], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['apt', 'install', 'wireguard', 'wireguard-tools', '-y'], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("WireGuard installed successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error installing WireGuard: {e}")

def generate_keys(key_dir, key_name):
    private_key_path = f"{key_dir}/{key_name}_private.key"
    public_key_path = f"{key_dir}/{key_name}_public.key"
    try:
        subprocess.run(['mkdir', '-p', key_dir], check=True)
        private_key = subprocess.run(['wg', 'genkey'], check=True, capture_output=True, text=True).stdout.strip()
        public_key = subprocess.run(['wg', 'pubkey'], input=private_key, text=True, capture_output=True).stdout.strip()

        subprocess.run(['bash', '-c', f'echo "{private_key}" > {private_key_path}'], check=True)
        subprocess.run(['bash', '-c', f'echo "{public_key}" > {public_key_path}'], check=True)

        print(f"Keys generated for {key_name}.")
    except subprocess.CalledProcessError as e:
        print(f"Error generating keys for {key_name}: {e}")

def get_ssh_client_ip():
    try:
        result = subprocess.run(['who'], capture_output=True, text=True, check=True)
        lines = result.stdout.splitlines()
        for line in lines:
            if '(:' in line:
                parts = line.split()
                for part in parts:
                    if part.startswith('(') and part.endswith(')'):
                        ip = part[1:-1]
                        return ip
        return None
    except subprocess.CalledProcessError as e:
        print(f"Error retrieving SSH client IP: {e}")
        return None

def configure_wireguard(client_ip):
    try:
        server_private_key = subprocess.run(['cat', '/etc/wireguard/server_private.key'], check=True, capture_output=True, text=True).stdout.strip()
        client_public_key = subprocess.run(['cat', '/etc/wireguard/client_public.key'], check=True, capture_output=True, text=True).stdout.strip()

        config_content = f"""
        [Interface]
        PrivateKey = {server_private_key}
        Address = 10.0.0.1/24
        ListenPort = 51820

        [Peer]
        PublicKey = {client_public_key}
        AllowedIPs = {client_ip}/32
        """

        subprocess.run(['bash', '-c', f'echo "{config_content}" > /etc/wireguard/wg0.conf'], check=True)
        subprocess.run(['chmod', '600', '/etc/wireguard/wg0.conf'], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("WireGuard configured successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error configuring WireGuard: {e}")

def enable_wireguard_service():
    try:
        subprocess.run(['systemctl', 'enable', 'wg-quick@wg0'], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['systemctl', 'start', 'wg-quick@wg0'], check=True, stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("WireGuard service enabled and started successfully.")
    except subprocess.CalledProcessError as e:
        print(f"Error enabling or starting WireGuard service: {e}")

def main():
    steps = [
        ("Install WireGuard", install_wireguard),
        ("Generate Server Keys", lambda: generate_keys('/etc/wireguard', 'server')),
        ("Generate Client Keys", lambda: generate_keys('/etc/wireguard', 'client')),
        ("Retrieve SSH Client IP", get_ssh_client_ip),
        ("Configure WireGuard", None),  # This will be set later after retrieving the client IP
        ("Enable WireGuard Service", enable_wireguard_service)
    ]

    client_ip = None

    for step_description, step_function in tqdm(steps, desc="Setting up WireGuard VPN", ncols=100):
        if step_function:
            result = step_function()
            if step_description == "Retrieve SSH Client IP":
                client_ip = result
                if not client_ip:
                    print("Failed to retrieve SSH client IP address. WireGuard configuration aborted.")
                    return
                else:
                    steps[4] = ("Configure WireGuard", lambda: configure_wireguard(client_ip))

    print("WireGuard VPN setup complete.")

if __name__ == "__main__":
    main()
