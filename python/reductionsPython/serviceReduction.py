# Necessary imports
import subprocess
from whiptail import Whiptail
from tqdm import tqdm

# Manage the services
w = Whiptail(title="Manage the services", backtitle="Reduction of system size")

# List of services to remove
services_to_remove = [
    "alsa-restore.service",
    "alsa-state.service",
    "alsa-utils.service",
    "apparmor.service",
    "bluetooth.service",
    "bthelper@hci0.service",
    "console-setup.service",
    "hciuart.service",
    "pigpiod.service",
    "triggerhappy.service"
]

# Function to list the installed services
def list_installed_services():
    """
    Lists the installed services.

    Returns:
        list: List of installed services.
    """
    command = "systemctl list-unit-files | grep enabled | awk '{ print $1 }'"
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    if result.returncode == 0:
        service_list = result.stdout.splitlines()
        return service_list
    else:
        return []

# Function to show a list dialog box
def show_service_list(service_list):
    """
    Displays a checklist dialog for selecting services.

    Args:
        service_list (list): List of available services.

    Returns:
        list: Selected services.
    """
    w = Whiptail(title="Select services to remove", backtitle="List of services")
    selected_services = w.checklist("Select services to remove", items=service_list)[0]
    return selected_services

# Function to remove the selected services
def remove_selected_services(selected_services):
    """
    Disables and stops the selected services.

    Args:
        selected_services (list): List of services to be removed.
    """
    for service in tqdm(selected_services, desc="Removing services", ncols=100):
        subprocess.run(['sudo', 'systemctl', 'disable', service], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'systemctl', 'stop', service], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

# Main script execution
def main():
    # Choice of execution mode (auto with predefined services or manual)
    choice, ok = w.menu("Choose the execution mode :", [("Auto", "Remove predefined services."), ("Manual", "Remove services manually.")])
    if ok:
        return
    
    if choice == "Auto":
        # Deletion of predefined services
        remove_selected_services(services_to_remove)
    else:
        # Get the list of installed services
        service_list = list_installed_services()
        selected_services = show_service_list(service_list)
        # Disable and stop selected services
        remove_selected_services(selected_services)

# Execute the main script
if __name__ == "__main__":
    main()
