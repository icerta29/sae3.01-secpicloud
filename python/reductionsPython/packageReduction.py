import subprocess
from tqdm import tqdm
from whiptail import Whiptail

# Delete packages
w = Whiptail(title="Delete the useless packages", backtitle="Reduction of system size")

# List of packages to remove
packages_to_remove = [
    "alsa-topology-conf",
    "alsa-ucm-conf",
    "alsa-utils",
    "apparmor",
    "bluez",
    "bluez-firmware",
    "bsdextrautils",
    "build-essential", 
    "cifs-utils",
    "cpp",
    "cpp-12",
    "dc",
    "debconf-i18n",
    "debconf-utils",
    "distro-info-data",
    "dmidecode",
    "dns-root-data",
    "dos2unix",
    "dphys-swapfile",
    "dpkg-dev",
    "ed",
    "eject",
    "exfatprogs",
    "fakeroot",
    "fbset",
    "file",
    "flashrom",
    "fontconfig-config",
    "fonts-dejavu-core",
    "g++",
    "g++-12",
    "gcc",
    "gcc-12",
    "gdb",
    "gdisk",
    "gettext-base",
    "groff-base",
    "install-info",
    "iso-codes",
    "javascript-common",
    "less",
    "logrotate",
    "lsb-release",
    "lua5.1",
    "luajit",
    "make",
    "man-db",
    "manpages",
    "manpages-dev",
    "mkvtoolnix",
    "modemmanager",
    "ncdu",
    "ncurses-term",
    "netcat-openbsd",
    "nfs-common",
    "ntfs-3g",
    "p7zip-full",
    "patch",
    "paxctld",
    "perl",
    "perl-modules-5.36",
    "pi-bluetooth",
    "pigpio",
    "pigpio-tools",
    "pigpiod",
    "pkexec",
    "pkg-config:arm64",
    "pkgconf:arm64",
    "pkgconf-bin",
    "policykit-1",
    "polkitd",
    "polkitd-pkla",
    "publicsuffix",
    "rfkill",
    "rpcbind",
    "rpcsvc-proto",
    "rsync",
    "runit-helper",
    "sgml-base",
    "shared-mime-info",
    "strace",
    "tasksel",
    "tasksel-data",
    "traceroute",
    "triggerhappy",
    "ucf",
    "udisks2",
    "usb-modeswitch",
    "usb-modeswitch-data",
    "v4l-utils",
    "vcdbg",
    "vim-common",
    "vim-tiny",
    "xauth",
    "xml-core",
    "xz-utils",
    "zip",
    "zlib1g-dev:arm64",
    "zstd",
]

# Function to list the installed packages
def list_installed_packages():
    """
    Lists the installed packages using dpkg.

    Returns:
        list: List of installed packages.
    """
    command = "dpkg --get-selections | grep -v deinstall | awk '{print $1}'"
    result = subprocess.run(command, shell=True, capture_output=True, text=True)
    if result.returncode == 0:
        package_list = result.stdout.splitlines()
        return package_list
    else:
        return []

# Function to show a list dialog box containing the list of packages
def show_package_list(package_list):
    """
    Displays a checklist dialog for selecting packages.

    Args:
        package_list (list): List of available packages.

    Returns:
        list: Selected packages.
    """
    w = Whiptail(title="Selecting packages to remove", backtitle="Packages list")
    selected_packages = w.checklist("Select the packages to delete", items=package_list)[0]
    return selected_packages

# Function to remove the selected packages
def remove_selected_packages(selected_packages):
    """
    Removes the selected packages.

    Args:
        selected_packages (list): List of packages to be removed.
    """
    for package in tqdm(selected_packages, desc="Removing packages", ncols=100):
        subprocess.run(['sudo', 'apt-get', 'remove', '-y', package], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    w = Whiptail(title="Packages delete", backtitle="Packages delete")
    subprocess.run(['sudo', 'apt-get', 'autoremove', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

# Main script execution
def main():
    # Choice of execution mode (auto with predefined packages or manual)
    choice, ok = w.menu("Choose the execution mode :", [("Auto", "Delete predefined packages."), ("Manual", "Manual delete of packages.")])
    if ok:
        return
    
    if choice == "Auto":
        # Deletion of predefined packages
        remove_selected_packages(packages_to_remove)
        print("Success: packages deleted.")
    elif choice == "Manual":
        # Get the list of installed packages
        package_list = list_installed_packages()

        if not package_list:
            print("No packages found.")
        else:
            selected_packages = show_package_list(package_list)

            if selected_packages:
                remove_selected_packages(selected_packages)
                subprocess.run(['sudo', 'apt-get', 'autoremove', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
                print("Success: packages deleted.")
            else:
                print("No package selected.")
    
    # Install essential packages (openssh-client, openssh-server) because they may have been deleted
    subprocess.run(['sudo', 'apt-get', 'install', '-y', 'openssh-client', 'openssh-server', '--fix-missing'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

# Execute the main script
if __name__ == "__main__":
    main()
