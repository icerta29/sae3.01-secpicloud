import subprocess
from whiptail import Whiptail
from tqdm import tqdm
import time

# Initialize Whiptail for displaying messages and dialog boxes
w = Whiptail(title="Recommandations", backtitle="Recommandations pour la sécurité du système")

# Securisation 1: IPv4 Network Configuration
def ipv4_network_configuration():
    """
    Securisation 1: IPv4 Network Configuration
    Configure various IPv4 network parameters for enhanced security.
    """
    try:
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.accept_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.log_martians=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.rp_filter=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.send_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.default.accept_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.default.accept_source_route=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.default.log_martians=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.core.bpf_jit_harden=2'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.ip_forward=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.accept_local=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.secure_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.default.secure_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.shared_media=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.default.shared_media=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.accept_source_route=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.arp_filter=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.arp_ignore=2'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.route_localnet=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.all.drop_gratuitous_arp=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.default.rp_filter=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.conf.default.send_redirects=0'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.icmp_ignore_bogus_error_responses=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.ip_local_port_range=32768 65535'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.tcp_rfc1337=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'sysctl', '-w', 'net.ipv4.tcp_syncookies=1'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error while configuring the IPv4 network: {e}")

# Securisation 4: Restrict Access to /boot
def restrict_access_to_boot():
    """
    Securisation 4: Restrict Access to /boot
    Configure access restrictions for the /boot directory.
    Only the root user can access the /boot directory.
    """
    try:
        # Verify who can access the /boot directory
        command = "ls -l /boot"
        result = subprocess.run(command.split(), stdout=subprocess.PIPE)
        output = result.stdout.decode('utf-8')
        
        if "root" in output:
            print("Only the root user can access the /boot directory.")
        else:
            print("Root user isn't the only one who can access the /boot directory.")
            print("Updating...")
            # Change the access rights of the /boot directory
            subprocess.run(['sudo', 'chmod', '700', '/boot'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
            print("Update successful.")
    except Exception as e:
        print(f"Error while restricting access to /boot: {e}")

# Securisation 5: Strong Password Policies
def strong_password_policies():
    """
    Securisation 5: Strong Password Policies
    Implement strong password policies as needed.
    """
    try:
        #Verify if pam pwquality is installed
        command = "dpkg -s libpam-pwquality"
        result = subprocess.run(command.split(), stdout=subprocess.PIPE)
        output = result.stdout.decode('utf-8')
        print("Installing...")
        subprocess.run(['sudo', 'apt', 'install', 'libpam-pwquality', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Installation successful.")
        # Apply the new password policy to /etc/pam.d/common-password
        line_to_add = "password requisite pam_pwquality.so retry=3 minlen=8 ucredit=-1 dcredit=-1 lcredit=-1 ocredit=-1"
        file_path = "/etc/pam.d/common-password"
        subprocess.run(['sudo', 'sed', '-i', f'$ a {line_to_add}', file_path], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print ("Updating the password policy successful.")
        # Update PAM authentication settings
        subprocess.run(['sudo', 'pam-auth-update'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Updating the pam-auth-update successful.")
        # Ask the 'secpicloud' user to change their password
        password_changed = False
        while not password_changed:
            # Prompt user for a new password
            password = w.inputbox("Please enter a new password for the secpicloud user (must have at least 8 characters, 1 uppercase letter, 1 lowercase letter, 1 number and 1 special character):")[0]
            # Confirm the new password
            password_confirmation = w.inputbox("Confirm the new password :")[0]
            # Validate password against the policy
            if password == password_confirmation and len(password) >= 8 and any(char.isupper() for char in password) and any(char.islower() for char in password) and any(char.isdigit() for char in password) and any(not char.isalnum() for char in password):
                password_changed = True
                # Change the user password
                subprocess.run(['sudo', 'passwd', 'secpicloud'], input=f"{password}\n{password}\n".encode(), stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
                print("Password changed successfully.")
            else:
                w.msgbox("The passwords don't match or don't respect the policy.")
                print("The passwords don't match or don't respect the policy.")
    except Exception as e:
        print(f"Error while implementing strong password policies: {e}")


# Securisation 6: Local Session Expiry
def local_session_expiry():
    """
    Securisation 6: Local Session Expiry
    Configure local session expiry settings.
    """
    try:
        # Set session expiry to 900 seconds (15 minutes) in /etc/bash.bashrc
        subprocess.run(['sudo', 'sh', '-c', 'echo "TMOUT=900" >> /etc/bash.bashrc'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Configuration of local session expiry successful.")
    except Exception as e:
        print(f"Error while configuring local session expiry: {e}")

# Securisation 7: Dedicated Group for sudo
def dedicated_group_for_sudo():
    """
    Securisation 7: Dedicated Group for sudo
    Create a dedicated group for sudo users as needed.
    """
    try:
        # Create the group
        subprocess.run(['sudo', 'groupadd', 'sudogrp'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Group created successfully.")
        # Add sudo rights to the group
        subprocess.run(['sudo', 'sh', '-c', 'echo "%sudogrp ALL=(ALL) ALL" >> /etc/sudoers'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Dedicated group for sudo users created successfully.")
    except Exception as e:
        print(f"Error while creating a dedicated group for sudo users: {e}")

# Securisation 10: Regular Updates
def configure_cron_job_update():
    """
    Securisation 10: Regular Updates
    Implement regular system updates as needed.
    """
    try:
        # Cron job configuration
        subprocess.run(['sudo', 'chmod', '+x', '/home/secpicloud/sae3.01-secpicloud/bash/update.sh'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'ln', '-s', '/home/secpicloud/sae3.01-secpicloud/bash/update.sh', '/etc/cron.daily/'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Regular updates configured successfully.")
    except Exception as e:
        print(f"Error while configuring regular updates: {e}")

# Securisation 12: Disable Unused Accounts
def configure_cron_job_delUser():
    """
    Securisation 12: Disable Unused Accounts
    Disable unused user accounts as needed.
    """
    try:
        # Cron job configuration
        subprocess.run(['sudo', 'chmod', '+x', '/home/secpicloud/sae3.01-secpicloud/bash/delUser.sh'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        subprocess.run(['sudo', 'ln', '-s', '/home/secpicloud/sae3.01-secpicloud/bash/delUser.sh', '/etc/cron.daily/'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Automatic deletion of unused accounts configured successfully.")
    except Exception as e:
        print(f"Error while configuring automatic deletion of unused accounts: {e}")

# Securisation 17: Available Antivirus
def configure_antivirus():
    """
    Securisation 17: Available Antivirus
    Install and configure an antivirus solution as needed.
    """
    try:
        # Install the antivirus solution (ClamAV)
        subprocess.run(['sudo', 'apt', 'install', 'clamav', '-y'])
        # Update the antivirus database
        subprocess.run(['sudo', 'freshclam'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Configure the antivirus solution
        subprocess.run(['sudo', 'dpkg-reconfigure', 'clamav'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Antivirus installed and configured successfully.")
        # Authorize the execution of the daily scan script
        subprocess.run(['sudo', 'chmod', '+x', '/home/secpicloud/sae3.01-secpicloud/bash/clamscan_daily.sh'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Link the daily scan script to cron.daily
        subprocess.run(['sudo', 'ln', '-s', '/home/secpicloud/sae3.01-secpicloud/bash/clamscan_daily.sh', '/etc/cron.daily/'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error while installing and configuring an antivirus solution: {e}")

# Securisation 18: New SSH Configuration
def new_ssh_configuration():
    """
    Securisation 18: New SSH Configuration
    Configure a new SSH configuration as needed.
    """
    try:
        # Backup the sshd_config file
        subprocess.run(['sudo', 'cp', '/etc/ssh/sshd_config', '/etc/ssh/sshd_config.bak'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Update the sshd_config file with the one in /home/secpicloud/sae3.01-secpicloud/others/sshd_config
        subprocess.run(['sudo', 'cp', '/home/secpicloud/sae3.01-secpicloud/others/sshd_config', '/etc/ssh/sshd_config'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Restart the SSH service
        subprocess.run(['sudo', 'systemctl', 'restart', 'ssh'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("New SSH configuration successful.")
    except Exception as e:
        print(f"Error while configuring a new SSH configuration: {e}")

# Securisation 30: Home directory encrypted wit PAM
def encrypt_home_directories():
    """
    Securisation 30: Home directory encrypted wit PAM
    Encrypt home directories with PAM as needed.
    """
    try:
        # Is the fscrypt package installed ?
        result = subprocess.run(['dpkg', '-s', 'libpam-fscrypt'])
        if result.returncode != 0:
            # If the fscrypt package isn't installed, install it
            subprocess.run(['sudo', 'apt', 'install', 'libpam-fscrypt', '-y'])
            print("fsCrypt package installed successfully.")
        # Setup the fscrypt package
        subprocess.run(['sudo', 'fscrypt', 'setup'])
        # Get the partition containing the /home directory
        result = subprocess.run(['mount', '-l', '|', 'grep', '/home'])
        # Crypt the partition containing the /home directory
        subprocess.run(['sudo', 'tune2fs', '-O', 'encrypt', result.stdout.decode('utf-8').split("\n")[0].split(" ")[0]])
        # Encrypt the /home directory of secpicloud
        subprocess.run(['sudo', 'fscrypt', 'encrypt', '/home/secpicloud'])
        # Lock the /home directory of secpicloud
        subprocess.run(['sudo', 'fscrypt', 'lock', '/home/secpicloud'])
        print("Home directories encrypted successfully.")
    except Exception as e:
        print(f"Error while encrypting home directories: {e}")

# Securisation 31: Restrict access range with PAM
def restrict_access_range():
    """
    Securisation 31: Restrict access range with PAM
    Restrict access range with PAM as needed.
    """
    try:
        # Install the libpam-modules package
        subprocess.run(['sudo', 'apt', 'install', 'libpam-modules', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Configure the PAM configuration file to restrict access range
        subprocess.run(["sudo", "sed", "-i", 's/^#account required pam_access.so/account required pam_access.so/', "/etc/pam.d/common-account"], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Access range restricted successfully.")
    except Exception as e:
        print(f"Error while restricting access range: {e}")

# Securisation 32: Firewall installation
def firewall_installation():
    """
    Securisation 32: Firewall installation
    Install and configure a firewall as needed.
    """
    try:
        # Replace the old firewall rules file with the new one
        subprocess.run(['sudo', 'cp', '/home/secpicloud/sae3.01-secpicloud/others/iptables.sh', '/etc/iptables.sh'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Apply the new firewall rules
        subprocess.run(['sudo', 'bash', '/etc/iptables.sh'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Enable the firewall service
        subprocess.run(['sudo', 'systemctl', 'enable', 'netfilter-persistent'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Restart the firewall service
        subprocess.run(['sudo', 'systemctl', 'restart', 'netfilter-persistent'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Firewall installed and configured successfully.")
    except Exception as e:
        print(f"Error while installing the firewall: {e}")

# Securisation 33: Implement DDoS protection
def install_fail2ban():
    """
    Securisation 33: Implement DDoS protection
    Implement DDoS protection as needed.
    """
    try:
        # Installer le paquet fail2ban
        subprocess.run(['sudo', 'apt', 'install', 'fail2ban', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Copier le fichier de configuration par défaut
        subprocess.run(['sudo', 'cp', '/etc/fail2ban/jail.conf', '/etc/fail2ban/jail.local'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Ajouter les règles de fail2ban en modifiant default-debian.conf
        subprocess.run(['sudo', 'sh', '-c', 'echo "[DEFAULT]\nallowipv6 = auto\n[sshd]\nlogpath = /var/log/auth.log\nenabled = true\nport = ssh\nbanaction = iptables-ipset-proto6\nbanaction_allports = iptables-ipset-proto6-allports\nmaxretry = 3\nbantime = 600" >> /etc/fail2ban/jail.d/default-debian.conf'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Fail2ban installed and configured successfully.")
    except Exception as e:
        print(f"Error while installing and configuring Fail2ban: {e}")

# Securisation 34: Implement Rootkit Detection
def installer_rkhunter_chrootkit():
    """
    Securisation 34: Implement Rootkit Detection
    Implement rootkit detection as needed.
    """
    try:
        # Installer le paquet rkhunter
        subprocess.run(['sudo', 'apt', 'install', 'rkhunter', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Installer le paquet chkrootkit
        subprocess.run(['sudo', 'apt', 'install', 'chkrootkit', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Mettre à jour la base de données de signatures
        subprocess.run(['sudo', 'rkhunter', '--update'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Rendre le fichier chrootkit_rkhunter_daily.sh exécutable
        subprocess.run(['sudo', 'chmod', '+x', '/home/secpicloud/sae3.01-secpicloud/bash/chrootkit_rkhunter_daily.sh'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Ajouter le lien symbolique à cron.daily
        subprocess.run(['sudo', 'ln', '-s', '/home/secpicloud/sae3.01-secpicloud/bash/chrootkit_rkhunter_daily.sh', '/etc/cron.daily/'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("Rkhunter & CHRootKit installed and configured successfully.")
    except Exception as e:
        print(f"Error while installing and configuring Rkhunter & CHRootKit: {e}")

# Securisation 35: Install apt-listbugs
def install_apt_listbugs():
    """
    Install apt-listbugs.
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'apt-listbugs', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error while installing apt-listbugs: {e}")

# Securisation 36: Install apt-listchanges
def install_apt_listchanges():
    """
    Install apt-listchanges.
    """
    try:
        subprocess.run(['sudo', 'apt', 'install', 'apt-listchanges', '-y'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
    except Exception as e:
        print(f"Error while installing apt-listchanges: {e}")

def new_interface_configuration():
    """
    Configure a new network interface. Using the others/interface file.
    """
    try:
        # Backup the interfaces file
        subprocess.run(['sudo', 'cp', '/etc/network/interfaces', '/etc/network/interfaces.bak'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Update the interfaces file with the one in /home/secpicloud/sae3.01-secpicloud/others/interfaces
        subprocess.run(['sudo', 'cp', '/home/secpicloud/sae3.01-secpicloud/others/interfaces', '/etc/network/interfaces'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        # Restart the network service
        subprocess.run(['sudo', 'systemctl', 'restart', 'networking'], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)
        print("New network interface configuration successful.")
    except Exception as e:
        print(f"Error while configuring a new network interface: {e}")


# Main script execution
def main():
    w = Whiptail(title="Security Script", backtitle="Security Script")
    w.msgbox("Welcome to the security script.")

    choice, ok = w.menu("Choose an execution mode :", [
        ("Automatic mode", "All Securisations will be applied automatically"),
        ("Manual mode", "You will be able to choose which Securisations to apply")
    ])

    if ok:
        return

    if choice == "Automatic mode":
        # Liste des fonctions de sécurisation
        securisation_functions = [
            ipv4_network_configuration,
            restrict_access_to_boot,
            strong_password_policies,
            local_session_expiry,
            dedicated_group_for_sudo,
            configure_cron_job_update,
            configure_cron_job_delUser,
            configure_antivirus,
            new_ssh_configuration,
            encrypt_home_directories,
            restrict_access_range,
            firewall_installation,
            install_fail2ban,
            installer_rkhunter_chrootkit,
            install_apt_listbugs,
            install_apt_listchanges,
            new_interface_configuration
        ]

        # Exécuter chaque fonction de sécurisation avec une barre de chargement
        for func in tqdm(securisation_functions, desc="Applying security measures", ncols=100):
            func()
            time.sleep(0.1)  # Simulate a short delay for better visualization of the progress bar
    elif choice == "Manual mode":
        # Liste des fonctions de sécurisation
        securisation_functions = [
            ("IPv4 Network Configuration", ipv4_network_configuration),
            ("Restrict Access to /boot", restrict_access_to_boot),
            ("Strong Password Policies", strong_password_policies),
            ("Local Session Expiry", local_session_expiry),
            ("Dedicated Group for sudo", dedicated_group_for_sudo),
            ("Configure Cron Job Update", configure_cron_job_update),
            ("Configure Cron Job DelUser", configure_cron_job_delUser),
            ("Configure Antivirus", configure_antivirus),
            ("New SSH Configuration", new_ssh_configuration),
            ("Encrypt Home Directories", encrypt_home_directories),
            ("Restrict Access Range", restrict_access_range),
            ("Firewall Installation", firewall_installation),
            ("Install Fail2ban", install_fail2ban),
            ("Install Rkhunter & Chrootkit", installer_rkhunter_chrootkit),
            ("Install apt-listbugs", install_apt_listbugs),
            ("Install apt-listchanges", install_apt_listchanges),
            ("New Interface Configuration", new_interface_configuration)
        ]

        # Demander à l'utilisateur de choisir les fonctions de sécurisation à appliquer
        choices, ok = w.checklist("Choose the security measures to apply:", securisation_functions)

        if ok:
            return

        # Exécuter chaque fonction de sécurisation sélectionnée
        for choice in choices:
            securisation_functions[int(choice)][1]()
    else:
        print("Invalid choice")

if __name__ == "__main__":
    main()
