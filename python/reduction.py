import subprocess

# Service reduction script
subprocess.run(['python3', 'python/reductionsPython/serviceReduction.py'])

# Package reduction script
subprocess.run(['python3', 'python/reductionsPython/packageReduction.py'])

# Purge unused packages
subprocess.run(['sudo', 'apt-get', 'autoremove', '-y'])
subprocess.run(['sudo', 'apt-get', 'clean', '-y'])