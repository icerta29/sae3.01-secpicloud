#!/bin/bash

# Update the system
sudo apt update && sudo apt upgrade -y

#Install Docker
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=arm64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install docker-ce docker-ce-cli containerd.io -y

sudo systemctl start docker
sudo systemctl enable docker

sudo usermod -aG docker ${USER}

# Create a directory for Nextcloud
mkdir ~/nextcloud-docker
cd ~/nextcloud-docker

# Create a docker-compose.yml file
cat <<EOF > docker-compose.yml
version: '3.7'

services:
  nextcloud:
    image: nextcloud
    container_name: nextcloud
    restart: always
    ports:
      - 80:80
    volumes:
      - nextcloud:/var/www/html
    environment:
      - MYSQL_HOST=db
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud
      - MYSQL_PASSWORD=nextcloud

  db:
    image: mariadb
    container_name: nextcloud-db
    restart: always
    volumes:
      - db:/var/lib/mysql
    environment:
      - MYSQL_ROOT_PASSWORD=rootpassword
      - MYSQL_DATABASE=nextcloud
      - MYSQL_USER=nextcloud
      - MYSQL_PASSWORD=nextcloud

volumes:
  nextcloud:
  db:
EOF

# Launch Docker Compose
sudo docker-compose up -d

echo "Nextcloud installation script with Docker is complete."
echo "Open your web browser and navigate to http://your_domain_or_ip to complete the setup."
