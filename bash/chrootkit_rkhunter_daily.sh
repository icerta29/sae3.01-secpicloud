#!/bin/bash
# ------------------------------
# Daily Security Check Script
# ------------------------------

# Purpose:
# This script performs daily security checks using chrootkit and rkhunter, and sends a report via email.

# Usage:
# 1. Make sure the script is executable: chmod +x daily_security_check.sh
# 2. Schedule it to run daily using a cron job.

# ------------------------------
# Chrootkit Daily Report
# ------------------------------

# Run chrootkit and send the report via email
sudo chrootkit | mail -s "Chrootkit Daily Report" secpicloud@$HOSTNAME

# ------------------------------
# RKHunter Daily Report
# ------------------------------

# Update RKHunter database
sudo rkhunter --update

# Update system file properties for RKHunter
sudo rkhunter --propupd

# Run RKHunter with all checks enabled and send the report via email
sudo rkhunter -c --enable all --disable none | mail -s "RKHunter Daily Report" secpicloud@$HOSTNAME

# ------------------------------
# End of Script
# ------------------------------
