#!/bin/bash
# Script: disable_inactive_users.sh
# Description: Disable inactive users who have not logged in for more than a month.

# ------------------------------
# Disable Inactive Users
# ------------------------------

# Loop through the list of users who have not logged in for more than a month
for user in $(lastlog -b 30 | awk 'NR>1 {print $1}'); do
    # Disable the user account
    sudo usermod -L $user
done

# ------------------------------
# Script Completion
# ------------------------------

# This script disables user accounts that have been inactive for more than a month.
# It uses the lastlog command to identify users who haven't logged in during the specified period.
# The usermod command is then used to lock (disable) each identified user account.

# Note: The script assumes the use of sudo for elevated privileges.
