#!/bin/bash
# Script: clamav_daily_scan.sh
# License: Copyleft free software

# ------------------------------
# Configuration
# ------------------------------
LOGFILE="/var/log/clamav/clamav-$(date +'%Y-%m-%d').log";
HOST="$(hostname --long)";
EMAIL_MSG="Please see the log file attached.";
EMAIL_FROM="clamav@"$HOST"";
EMAIL_TO="bob@"$HOST"";
DIRTOSCAN="/home";

# Check for mail installation
type mail >/dev/null 2>&1 || { echo >&2 "I require mail but it's not installed. Aborting."; exit 1; };

# ------------------------------
# Update ClamAV database
# ------------------------------
echo "Looking for ClamAV database updates...";
freshclam --quiet;

# ------------------------------
# Perform ClamAV Scan
# ------------------------------
TODAY=$(date +%u);

if [ "$TODAY" == "6" ]; then
    echo "Starting a full weekend scan.";
    # be nice to others while scanning the entire root
    nice -n5 clamscan -ri / --exclude-dir=/sys/ &>"$LOGFILE";
else
    DIRSIZE=$(du -sh "$DIRTOSCAN" 2>/dev/null | cut -f1);
    echo -e "Starting a daily scan of "$DIRTOSCAN" directory.\nAmount of data to be scanned is "$DIRSIZE".";
    clamscan -ri "$DIRTOSCAN" &>"$LOGFILE";
fi

# ------------------------------
# Check for Malware
# ------------------------------
# Get the value of "Infected lines"
MALWARE=$(tail "$LOGFILE" | grep Infected | cut -d" " -f3);

# If the value is not equal to zero, send an email with the log file attached
if [ "$MALWARE" -ne "0" ]; then
    echo "$EMAIL_MSG" | mail -a "$LOGFILE" -s "ClamAV: Malware Found" -r "$EMAIL_FROM" "$EMAIL_TO";
    # Alternatively, you can use sendmail:
    # sendmail -f "$EMAIL_FROM" -t "$EMAIL_TO" -u "[ClamAV] Malware found!" -m "$EMAIL_MSG" -a "$LOGFILE"
fi

# ------------------------------
# Script Completion
# ------------------------------
echo "The script has finished.";
exit 0;
