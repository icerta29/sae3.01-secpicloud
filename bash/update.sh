#!/bin/bash
# Script: update_system.sh
# Description: Update the system by performing package updates and upgrades.

# ------------------------------
# Update the System
# ------------------------------

# Update the package list
sudo apt update -y

# Upgrade installed packages
sudo apt upgrade -y

# Perform distribution upgrade
sudo apt dist-upgrade -y

# Remove unused packages
sudo apt autoremove -y

# ------------------------------
# Script Completion
# ------------------------------

# This script updates the system by performing the following steps:
# 1. Update the package list to fetch information about available updates.
# 2. Upgrade installed packages to the latest versions.
# 3. Perform a distribution upgrade to handle changes in dependencies.
# 4. Remove unused packages to free up disk space.

# Note: The use of sudo assumes that the script is executed with elevated privileges.
