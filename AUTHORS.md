# Authors

1. **Ménez Matthys**
   - *Email:* menezmatthys@gmail.com
   - *Affiliation:* University of Vannes - PEI
   - *Contributions:* Scrum master, initial design, programming, report writing, testing, code optimization, documentation

2. **Francois Alexis**
   - *Email:* e2201042@etud.univ-ubs.fr
   - *Affiliation:* University of Vannes - PEI
   - *Contributions:* initial design, programming, report writing, testing, code optimization, documentation

3. **de Chalendar Alice**
   - *Email:* de-chalendar.e2200235@etud.univ-ubs.fr
   - *Affiliation:* University of Vannes - PEI
   - *Contributions:* initial design, programming, report writing, testing, code optimization, documentation

4. **Chaveneau Hugo**
   - *Email:* chaveneau.e2201421@etud.univ-ubs.fr
   - *Affiliation:* University of Vannes - PEI
   - *Contributions:* initial design, programming, report writing, testing, code optimization, documentation

# Acknowledgements

We would like to express our gratitude to everyone who contributed in any way to the realization of this project.