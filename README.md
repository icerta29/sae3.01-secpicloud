## SAE3.01 SecPiCloud

## Name
Raspberry Pi OS Security Project

## Description
Raspberry Pi devices are single-board nano-computers equipped with ARM processors. The Model B is the most common and is widely used for setting up personal servers (Web, backup, etc.) and multimedia centers (e.g., OSMC, LibreELEC). Several distributions are available for these nano-computers, including Raspberry Pi OS, DietPi OS, and Armbian, all based on the Debian distribution.

The goal of this project is to develop several scripts in Bash or Python to secure and reinforce the configuration of the Raspberry Pi OS system, the default OS offered by the Raspberry Pi Foundation.

## Specifications
Within this project, the following tasks must be accomplished by creating four scripts:

A script to remove unnecessary packages to bring the distribution closer to the efficiency of DietPi OS.

A script to secure and reinforce the configuration of the Raspberry Pi OS by performing the following actions:

Change the SSH port.
Allow SSH access by keys only.
Prohibit connections as root.
Encrypt user directories.
Implement a firewall (nftables).
Implement a HIDS (RKHunter, Fail2Ban, etc.).
A script to install and configure a WireGuard VPN, while updating the firewall rules accordingly.

A script to install and secure an Apache HTTP server hosting a NextCloud instance. This script should also update the firewall rules. The NextCloud instance may optionally be hosted within a Docker container.

All scripts should be user-friendly for non-expert users.

You can refer to the Linux security guide and collaborate with the SecPiNAS project for creating security scripts.

## Installation
To install the project, please follow the following steps : 

1) **Install a Raspberry Pi Os Lite image**
To start the utilisation of our project, we highly recommand you to use an official operating system : "Raspberry Pi OS Lite 64bits". You can install it on a SD Card using the official Raspberry Imager 

We also recommand you to set up a SSH configuration, you will be able to use it to install the different element of SecPiCloud using an external connection rather than install it directly from the Raspberry Pi

**Warning :**
When the Rasberry is booted, if ssh does not restart automatically, it must be restarted directly on the Rasberry 
with the command : 
   ```shell
   sudo systemctl restart ssh
   ```
If asked during the installation, don't forget to type Y + Enter if any package needs it.

Our securisation script will able the key authentification to prevent intrusion passing by the ssh server, if you dont want to get disconnected and you want this security to be setted up, please do these commands :

   ```shell
   # Start by obtaining the IP address of your RaspberryPi on the administrator interface of your internet box

   # Create your rsa key with the following command
   ssh-keygen -b 2048 -t rsa

   # Then send it to the ssh server
   ssh-copy-id -i ~/.ssh/id_rsa.pub secpicloud@[the_raspberry_ip]
   
   # Now, you can connect to the RaspberryPI :
   ssh secpicloud@[the_raspberry_ip]
   ```



2) **Clone the project on the Raspberry Pi**

Clone SecPiCloud using the following command on your Raspberry Pi/your ssh connection
   ```shell
   # Git installation
   sudo apt install -y git

   # Git initialization
   git init

   # Clone SecPiCloud
   git clone https://gitlab.com/projets-iut2/sae3.01-secpicloud.git
   ```

3) **Execute the init.sh file**

Now that SecPiCloud is cloned, you should use the init.sh file, it's a file that execute the important scripts to setup the project
   ```shell
   # GAuthorise the init.sh file to execute
   cd sae3.01-secpicloud/
   chmod u+x init.sh

   # Execution of init.sh
   ./init.sh
   ```

## Usage
The project aims to automate common configuration and security tasks, providing a comprehensive and simplified solution to deploy a secure Raspberry Pi with features such as VPN, web server, and online storage. These scripts can be used by non-expert users to quickly and effectively configure their Raspberry Pi in accordance with security best practices.

To achieve this, several scripts are implemented:


1) Script to clean unnecessary packages:

**Objective**: Optimize disk space usage by removing unnecessary packages.

**Usage**: After the initial OS installation, periodically running this script frees up space by removing obsolete or unnecessary packages, ensuring more efficient memory usage.

Here are our information about number of packages and size before deletion : 

![before deletion number of package](images/Avant.png)
![before deletion size](images/taille%20avant.png)

And after deletion : 

![after deletion number of package](images/Apres.png)
![after deletion size](images/Apres%20taille.png)


2) Script to secure Raspberry Pi OS:

**Objective**: Strengthen the security of Raspberry Pi OS by applying recommended practices.

**Usage**: This script is used after the initial installation to perform various security tasks, including changing the SSH port to prevent automated attacks, restricting SSH access to keys only for enhanced authentication, disabling root login to limit vulnerabilities, encrypting user directories, setting up a firewall (nftables) to control network traffic, and installing a HIDS (RKHunter, Fail2Ban) to detect and prevent intrusions.

3) Script to install and configure WireGuard VPN:

**Objective**: Set up a secure Virtual Private Network (VPN) with WireGuard.

**Usage**: This script is used to install and configure WireGuard, a modern VPN protocol. It also handles updating firewall rules to allow secure traffic via the VPN.

4) Script to install and secure Apache for NextCloud:

**Objective**: Deploy a secure Apache server to host a NextCloud instance, an online storage solution.

**Usage**: This script installs Apache and NextCloud, configures Apache security (e.g., SSL), and adjusts firewall rules accordingly. It also offers the option to host NextCloud within a Docker container, simplifying application and dependency management.
Support
For more information or assistance, solutions can be found on various forums available online.

## Authors and Acknowledgment
Powered by HAMA (Hugo CHAVENEAU, Alexis FRANCOIS, Matthys MENEZ, Alice DE CHALENDAR)

## License
It's an open-source project.

## Project Status
The project has been in development since October 2023.

