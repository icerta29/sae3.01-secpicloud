#!/bin/bash

# Effacer toutes les règles existantes
sudo iptables -F
sudo iptables -X
sudo iptables -t nat -F
sudo iptables -t nat -X
sudo iptables -t mangle -F
sudo iptables -t mangle -X
sudo iptables -t raw -F
sudo iptables -t raw -X

# Définir les politiques par défaut
sudo iptables -P INPUT DROP
sudo iptables -P FORWARD DROP
sudo iptables -P OUTPUT ACCEPT

# Accepter tout le trafic provenant de localhost
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A OUTPUT -o lo -j ACCEPT

# Accepter les connexions établies et relatives
sudo iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A OUTPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

# Autoriser DNS
sudo iptables -A INPUT -p udp --dport 53 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 53 -j ACCEPT
sudo iptables -A OUTPUT -p udp --sport 53 -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 53 -j ACCEPT

# Autoriser SSH (port 2222)
sudo iptables -A INPUT -p tcp --dport 2222 -m conntrack --ctstate NEW -m limit --limit 15/minute --limit-burst 5 -j DROP
sudo iptables -A INPUT -p tcp --dport 2222 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 2222 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Autoriser HTTP et HTTPS
sudo iptables -A INPUT -p tcp --dport 80 -m conntrack --ctstate NEW -m limit --limit 100/second --limit-burst 10 -j DROP
sudo iptables -A INPUT -p tcp --dport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 80 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW -m limit --limit 100/second --limit-burst 10 -j DROP
sudo iptables -A INPUT -p tcp --dport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 443 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Autoriser VPN (WireGuard sur le port 51821)
sudo iptables -A INPUT -p udp --dport 51821 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p udp --sport 51821 -j ACCEPT

# Autoriser ICMP (ping)
sudo iptables -A INPUT -p icmp -m conntrack --ctstate NEW -m limit --limit 20/second --limit-burst 5 -j ACCEPT
sudo iptables -A INPUT -p icmp -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
sudo iptables -A OUTPUT -p icmp -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Autoriser Docker (pour NextCloud)
sudo iptables -A INPUT -p tcp --dport 8080 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p udp --dport 8080 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 8080 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT
sudo iptables -A OUTPUT -p udp --sport 8080 -m conntrack --ctstate NEW,ESTABLISHED -j ACCEPT

# Droper tout le reste
sudo iptables -A INPUT -j LOG --log-prefix "iptables: " --log-level 7
sudo iptables -A INPUT -j DROP

# Enregistrer les règles pour les appliquer au démarrage
sudo apt install iptables-persistent -y
sudo netfilter-persistent save
